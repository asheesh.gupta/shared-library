def call()
{
    Deployment()
}

def Deployment()
{
    checkout scm
    readprop = readProperties file: "${workspace}/config.properties"
    input_func()
    echo "Packaged Downloaded"
    if( "${readprop.DEPLOYMENT}" == "NORMAL")
    {
        Normal()
    }
    if( "${readprop.DEPLOYMENT}" == "ROLLING")
    {
        Rolling()
    }
    if( "${readprop.DEPLOYMENT}" == "BLUE_GREEN")
    {
        Blue_Green()
    }
}

def input_func()
{
    def name = input message: '', parameters: [[$class: 'ExtensibleChoiceParameterDefinition', choiceListProvider: [$class: 'Nexus3ChoiceListProvider', artifactId: 'payslip', classifier: '', credentialsId: 'Nexus', groupId: 'slip', packaging: 'war', repositoryId: 'SAMPLE-SNAP', url: 'http://localhost:8081'], description: '', editable: false, name: 'SL']]
    sh """wget "${name}" """ 
}

def Normal()
{   
    def name=sh(script:"""ls *war""",returnStdout:true).trim()
    def artifact_name = sh(script:""" echo "$name" > abc.txt ; cat abc.txt | awk -F . '{print \$1}' """,returnStdout:true).trim()
    echo "-------------------------------------------------------------------------------------------------------------------------------"
    println artifact_name
    def nginx_data = readFile(file: 'nginx_server.txt')
    def lines = nginx_data.readLines()
    for (line in lines) 
    {
        (remote)=extract_data(line)
        sshCommand remote: remote, command:""" sudo systemctl stop nginx """
        echo "Reading Server file"
        def server_data = readFile(file: 'server.txt')
        def ser_line = server_data.readLines()
        for (line1 in ser_line)
        {
             (remote1 , ip , user,server_status,destination_path,port,file_name)=extract_data(line1)
             echo "Backing up"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             sshGet remote: remote1, from:"$destination_path*.${readprop.PACKAGE}" , into: './normal/', override: true
             echo "Removing"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             sshRemove remote: remote1, path:"$destination_path*.${readprop.PACKAGE}"
             echo "Deploying"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             sshPut remote: remote1, from: "$name" , into: "$destination_path"
             sleep(5)
             echo "Getting Server status"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             echo " ---------------------------------------------------------------------------------------------------------------------------"
             def commandResult = sshCommand remote: remote1, command: """curl -I --request GET http://$ip:$port/$artifact_name/ | grep ${readprop.HEALTH_CHECK_STATUS} | wc -l """
             echo "Result: " + commandResult
             println commandResult
             def package_name =sh(script: """ cd normal/ ; ls *.${readprop.PACKAGE}""",returnStdout:true).trim()
             println package_name
             def result=commandResult.toInteger()
         if( result == 1)
         {
             echo " Successfully Deployed and Working Fine"
             sshCommand remote: remote, command:""" cd /etc/nginx/conf.d; sudo sed -i 's~http://* ~http://$ip:$port/$artifact_name/ ;~g' $file_name """
             sh "sudo rm normal/$package_name"
             
         }
         else
         {
              echo "Rolling Back to the previous version"
              sshPut remote: remote1, from:"./normal/$package_name" , into: "$destination_path"
              echo " Successfully rolled back"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              sshRemove remote: remote1, path:"$destination_path$name"
              sh "sudo rm normal/$package_name"
              
             
         }
         
         echo " ---------------------------------------------------------------------------------------------------------------------------"
         echo " ---------------------------------------------------------------------------------------------------------------------------"
        }
        
        sh """sudo rm "${name}" """
        
        sshCommand remote: remote, command:'sudo systemctl restart nginx.service'
        sshCommand remote: remote, command:'sudo systemctl restart nginx'
        
    }
}


def Rolling()
{
    def name=sh(script:"ls *war",returnStdout:true).trim()
    def artifact_name = sh(script:""" sudo echo "$name" > abc.txt ; cat abc.txt | awk -F . '{print \$1}' """,returnStdout:true).trim()
    def nginx_data = readFile(file: 'nginx_server.txt')
    def lines = nginx_data.readLines()
    for (line in lines) 
    { 
        (remote)=extract_data(line)
        def server_data = readFile(file: 'server.txt')
        def ser_line = server_data.readLines()
        for (line1 in ser_line)
        {
              (remote1,ip_add,user,server_status,destination_path,port,file_name)=extract_data(line1)
              echo "chnaging conf file"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              sshCommand remote: remote, command:""" cd /etc/nginx ; sudo sed -i 's/$ip_add.*/$ip_add down ;/g' nginx.conf  """
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " BACKING UP"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              sshGet remote: remote1, from: "$destination_path/*.${readprop.PACKAGE}", into: './rolling/', override: true
              echo "Removing"
              sshRemove remote: remote1, path:"$destination_path*.${readprop.PACKAGE}"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " DEPLOYING "
              sshPut remote: remote1, from: "$name", into: "$destination_path"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              def package_name = sh(script: """ cd rolling/; sudo ls *.${readprop.PACKAGE}""",returnStdout:true).trim()
              def output = sshCommand remote: remote1, command: """ curl -I http://$ip_add:$port/$artifact_name/ | grep ${readprop.HEALTH_CHECK_STATUS} | wc -l """
              def status=output.toInteger()
              if (status == 1 )
              {
                  echo "Successfully deployed and Working fine"
                  sh "sudo rm rolling/$package_name"
                  sshCommand remote: remote, command:""" pwd ; sudo sed -i 's/$ip_add.*/$ip_add:$port ;/g' nginx.conf  """
              }
              else
              {
                  echo " Rolling Back"
                  sshRemove remote: remote1, path:"$destination_path$name"
                  sshPut remote: remote1, from:"./rolling/$package_name" , into: "$destination_path"
                  sshCommand remote: remote, command:""" cd /etc/nginx ; sudo sed -i 's/$ip_add.*/$ip_add:$port ;/g' nginx.conf  """
                  
                  sh "sudo rm rolling/$package_name"
              }
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              echo " ---------------------------------------------------------------------------------------------------------------------------"
              
        }
    }
}

def Blue_Green()
{
        def name=sh(script:"ls *war",returnStdout:true).trim()
        def artifact_name = sh(script:""" sudo echo "$name" > abc.txt ; cat abc.txt | awk -F . '{print \$1}' """,returnStdout:true).trim()
        def server_data = readFile(file: 'server.txt')
        def ser_line = server_data.readLines()
        for (line1 in ser_line)
        {
            (remote1,ip_add,user_name,count,destination_path,port,file_name)=extract_data(line1)
            echo "$count"
            if( count >= 1)
            {
            echo " ---------------------------------------------------------------------------------------------------------------------------"
            echo " ---------------------------------------------------------------------------------------------------------------------------"
            echo " BACKING UP"
            sshGet remote: remote1, from: "$destination_path/*.${readprop.PACKAGE}", into: './Blue_Green/', override: true
            echo "Removing"
            sshRemove remote: remote1, path:"$destination_path*.${readprop.PACKAGE}"
            echo " ---------------------------------------------------------------------------------------------------------------------------"
            echo " ---------------------------------------------------------------------------------------------------------------------------"
            echo " DEPLOYING "
            sshPut remote: remote1, from: "$name", into: "$destination_path"
            echo " ---------------------------------------------------------------------------------------------------------------------------"
            echo " ---------------------------------------------------------------------------------------------------------------------------"
            def package_name = sh(script: """ cd Blue_Green/; sudo ls *.${readprop.PACKAGE}""",returnStdout:true).trim()
            echo "STATUS CHECKING"
            sleep(5)
            def output = sshCommand remote: remote1, command: """ curl -I http://$ip_add:$port/$artifact_name/ |grep ${readprop.HEALTH_CHECK_STATUS} | wc -l"""
            def status=output.toInteger()
            if(status == 1)
            {
                 def nginx_data = readFile(file: 'nginx_server.txt')
                 def lines = nginx_data.readLines()
                 for (line in lines) 
                 { 
                    (remote)=extract_data(line)
                    echo " ---------------------------------------------------------------------------------------------------------------------------"
                    echo " ---------------------------------------------------------------------------------------------------------------------------"
                    echo "CHANGING THE CONFIG FILE"
                    sshGet remote: remote, from: '/etc/nginx/nginx.conf', into: './check/', override: true
                    def check =sh(script: """ cat check/nginx.conf | grep green_deploy | wc -l """,returnStdout:true)
                    echo "check value"
                    println check
                    def state=check.toInteger()
                    if( state >= 1)
                    {
                        echo " in if loop"
                        sshCommand remote: remote, command:"""cd /etc/nginx/ ;  sudo sed -i -e 's|blue_deploy|temp_deploy| ; s|green_deploy|blue_deploy| ; s|temp_deploy|green_deploy|' nginx.conf """        
                        sh 'sudo rm Blue_Green/*.war'
                        
                    }
                    else
                    {
                        echo " in else loop"
                        sshPut remote: remote, from:'nginx.conf', into: "/etc/nginx/"
                        sshPut remote: remote, from:'blue_green.conf', into: "/etc/nginx/conf.d/"
                        sshCommand remote: remote, command:"""cd /etc/nginx/ ;  sudo sed -i -e 's|blue_deploy|temp_deploy| ; s|green_deploy|blue_deploy| ; s|temp_deploy|green_deploy|' nginx.conf """ 
                    } 
                }
            }
            else
            {
                echo "LOOKS LIKE A PROBLEM HAS OCCURED "
                echo "Rolling Back to the previous version"
                sshRemove remote: remote1, path:"$destination_path$name"
                sshPut remote: remote1, from:"./Blue_Green/$package_name" , into: "$destination_path"
                sh "sudo rm Blue_Green/$package_name"
                
            }
            }
        else
        {
            echo "---------------------------------------------------------------------------------------------------------------------------"
            echo "---------------------------------------------------------------------------------------------------------------------------"
            echo " NO GREEN SERVERS FOUND"
                
        }
            
    }
}

def extract_data(line)
{

 try
    {
        
        
         
            def user_name=sh(script: """ echo "$line" | awk -F , ' {print \$2 }' """,returnStdout:true).trim()
            def ip_addr=sh(script: """echo "$line" | awk -F , ' {print \$3 }' """,returnStdout:true).trim()
            def private_key_path=sh(script: """echo "$line" | awk -F , ' {print \$4 }' """,returnStdout:true).trim()
            def destination_path=sh(script: """echo "$line" | awk -F , ' {print \$5 }' """,returnStdout:true).trim()
            def port=sh(script: """echo "$line" | awk -F , ' {print \$6 }' """,returnStdout:true).trim()
            def file_name=sh(script: """echo "$line" | awk -F , ' {print \$8 }' """,returnStdout:true).trim()
            def green_server=sh(script: """echo "$line"  | grep green | wc -l """,returnStdout:true).trim()
            def server_status=green_server.toInteger()
            def remote = [:]
                remote.name = "$user_name"
                remote.host = "$ip_addr"
                remote.user = "$user_name"
                // remote.password = 'asheesh'
                remote.identityFile = "$private_key_path"
                remote.allowAnyHosts = true
    
           
            return [remote ,ip_addr,user_name ,server_status,destination_path,port,file_name]
            
        
}
catch(error)
{
 echo "error"
 throw e
}
}
